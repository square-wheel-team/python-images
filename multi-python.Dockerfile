FROM ubuntu:20.04

RUN apt-get update && DEBIAN_FRONTEND=noninteractive apt-get install -y \
    --no-install-recommends software-properties-common build-essential libffi-dev
RUN add-apt-repository ppa:deadsnakes/ppa
RUN apt-get update
RUN apt-get install -y --no-install-recommends python3-pip
RUN apt-get install -y --no-install-recommends python3.6
RUN apt-get install -y --no-install-recommends python3.7
RUN apt-get install -y --no-install-recommends python3.8
RUN apt-get install -y --no-install-recommends python3.9
RUN apt-get install -y --no-install-recommends python3.10 python3.10-distutils python3.10-dev

ENV LANG C.UTF-8
